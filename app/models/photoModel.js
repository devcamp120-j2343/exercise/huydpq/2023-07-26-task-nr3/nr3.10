//Khai báo thư viện Mongoose:
const mongoose = require('mongoose');

//Khai báo thu viện Schema của mongoose:
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:

const photoModel = new Schema({
    _id: mongoose.Types.ObjectId,
    albumId: {
        type: mongoose.Types.ObjectId,
        ref: "album",
        required: true
    },
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    thumbnailUrl: {
        type: String,
        required: true
    },
   
})
//export model
module.exports = mongoose.model("photo", photoModel)