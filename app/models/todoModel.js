//Khai báo thư viện Mongoose:
const mongoose = require('mongoose');

//Khai báo thu viện Schema của mongoose:
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:

const toDoSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "user",
        required: true
    },
    title: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        required: true
    }
   
   
})
//export model
module.exports = mongoose.model("toDo", toDoSchema)