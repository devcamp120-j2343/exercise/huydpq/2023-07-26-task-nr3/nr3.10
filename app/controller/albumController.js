//Import model
const { default: mongoose } = require('mongoose');
const albumModel = require('../models/albumModel')
const userModel = require("../models/userModel")

//create new album
const createAlbum = async (req, res) => {
    //Thu thập dữ liệu:
    const {userId, title} = req.body;

    //validate dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }
    if (!title) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Titile is required !`
        })
    }
    //thực thi model:
    const newAlbumCreateData = {
        _id: new mongoose.Types.ObjectId,
        userId,
        title
    }
    try {
        const user = await userModel.findById(userId)
        if (user) {
            const newAlbumCreated = await albumModel.create(newAlbumCreateData)
            return res.status(201).json({
                status: `Create new album successfully !`,
                data: newAlbumCreated
            })
        } else {
            return res.status(400).json({
                status: "Not found any user"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all Album:
const getAllAlbums = async (req, res) => {
    const userId = req.query.userId
    try {
        if (userId) {
            const albumList = await albumModel.find({ userId: userId });
            if (albumList && albumList.length > 0) {
                return res.status(200).json({
                    status: "Get all Albums successfully!",
                    data: albumList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any Albums",
                    data: albumList

                })
            }
        }else{
            const AlbumList = await albumModel.find();
            if (AlbumList && AlbumList.length > 0) {
                return res.status(200).json({
                    status: "Get all Albums successfully!",
                    data: AlbumList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any Albums",
                    data: AlbumList

                })
            }
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get Album by id:
const getAlbumById = async (req, res) => {
    const albumId = req.params.albumId;
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Album Id ${albumId} is invalid`
        })
    }
    try {
        const albumFoundById = await albumModel.findById(albumId);
        if (albumFoundById) {
            return res.status(200).json({
                status: `Album found by Id ${albumId} is successfully !`,
                data: albumFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Albums by id ${albumId}`,
                data: albumFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


//Update Album by id
const updateAlbumById = async (req, res) => {
    const albumId = req.params.albumId;
    const { userId, title } = req.body;
        //validate:

    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Album Id ${albumId} is invalid !`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }

    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required !"
        })
    }
    

    const AlbumUpdateData = {
        userId,
        title
        
    }
    try {
        const newAlbumUpdated = await albumModel.findByIdAndUpdate(albumId, AlbumUpdateData);
        if (newAlbumUpdated) {
            return res.status(200).json({
                status: `Update Album by id ${albumId} successfully !`,
                data: newAlbumUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any Albums by Id ${albumId}`,
                data: newAlbumUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete Album by id
const deleteAlbumById = async (req, res) => {
    const albumId = req.params.albumId;
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Album Id ${albumId} is invalid !`
        })
    }
    try {
        const albumDeleted = await albumModel.findByIdAndDelete(albumId)
        if (albumDeleted) {
            return res.status(204).json({
                status: `Delete Album by id ${albumId} successfully`,
                data: albumDeleted
            })
        } else {
            return res.status(404).json({
                status: `Not found any Albums by Id ${albumId}`,
                data: albumDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
module.exports = {createAlbum, getAllAlbums, getAlbumById, updateAlbumById, deleteAlbumById}