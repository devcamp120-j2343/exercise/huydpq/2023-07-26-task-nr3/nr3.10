//Import model
const { default: mongoose } = require('mongoose');
const toDoModel = require("../models/todoModel")
const userModel = require("../models/userModel")
//create new toDo
const createToDo = async (req, res) => {
    //Thu thập dữ liệu:
    const { userId, title, completed } = req.body;

    //validate dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }
    if (!title) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Title is required !`
        })
    }
    if (!completed) {
        return res.status(400).json({
            status: `Bad request`,
            message: `completed is required !`
        })
    }

    //thực thi model:
    const newToDoCreateData = {
        _id: new mongoose.Types.ObjectId,
        userId,
        title,
        completed

    }
    try {
        const user = await userModel.findById(userModel);
       
        if (user) {
            const newToDoCreated = await toDoModel.create(newToDoCreateData)
            return res.status(201).json({
                status: `Create new ToDo successfully !`,
                data: newToDoCreated
            })
        } else {
            return res.status(400).json({
                status: "Not Found Any User",
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all ToDo:
const getAllToDos = async (req, res) => {
    const userId = req.query.userId
    try {
        if (userId) {
            const toDosList = await toDoModel.find({ userId: userId });
            if (toDosList && toDosList.length > 0) {
                return res.status(200).json({
                    status: `Get all ToDos of user id ${userId} successfully!`,
                    data: toDosList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any ToDos",
                    data: toDosList

                })
            }
        } else {
            const toDosList = await toDoModel.find();
            if (toDosList && toDosList.length > 0) {
                return res.status(200).json({
                    status: "Get all ToDos successfully!",
                    data: toDosList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any ToDos",
                    data: toDosList

                })
            }
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get ToDo by id:
const getToDoById = async (req, res) => {
    const toDoId = req.params.toDoId;
    if (!mongoose.Types.ObjectId.isValid(toDoId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `ToDo Id ${toDoId} is invalid`
        })
    }
    try {
        const toDoFoundById = await toDoModel.findById(toDoId);
        if (toDoFoundById) {
            return res.status(200).json({
                status: `ToDo found by Id ${toDoId} is successfully !`,
                data: toDoFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any ToDos by id ${toDoId}`,
                data: toDoFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update ToDo by id
const updateToDoById = async (req, res) => {
    const toDoId = req.params.toDoId;
    const { userId, title, completed } = req.body;
        //validate:

    if (!mongoose.Types.ObjectId.isValid(toDoId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `ToDo Id ${toDoId} is invalid !`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }

    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required !"
        })
    }
    if (!completed) {
        return res.status(400).json({
            status: `Bad request`,
            message: `completed is required !`
        })
    }
    const ToDoUpdateData = {
        userId,
        title,
        completed
        
    }
    try {
        const newToDoUpdated = await toDoModel.findByIdAndUpdate(toDoId, ToDoUpdateData);
        if (newToDoUpdated) {
            return res.status(200).json({
                status: `Update ToDo by id ${toDoId} successfully !`,
                data: newToDoUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any ToDos by Id ${toDoId}`,
                data: newToDoUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete ToDo by id
const deleteToDoById = async (req, res) => {
    const toDoId = req.params.toDoId;
    if (!mongoose.Types.ObjectId.isValid(toDoId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `ToDo Id ${toDoId} is invalid !`
        })
    }
    try {
        const toDoDeleted = await toDoModel.findByIdAndDelete(toDoId)
        if (toDoDeleted) {
            return res.status(204).json({
                status: `Delete ToDo by id ${toDoId} successfully`,
                data: toDoDeleted
            })
        } else {
            return res.status(404).json({
                status: `Not found any ToDos by Id ${toDoId}`,
                data: toDoDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = { createToDo, getAllToDos, getToDoById, updateToDoById, deleteToDoById  }