const { default: mongoose } = require("mongoose");
const userModel = require("../models/userModel");

const createUser = async (req, res) => {
    //thu thập dữ liệu:
    const { name,
        username,
        email,
        address: {
            street,
            suite,
            city,
            zipcode,
            geo: {
                lat,
                lng
            }
        },
        phone,
        website,
        company: {
            companyname,
            catchPhrase,
            bs
        }
    } = req.body;

    // //B2: validate dữ liệu:
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required !"
        })
    }
    if (!username) {
        return res.status(400).json({
            status: "Bad request",
            message: "username is required !"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required !"
        })
    }
    if (!street) {
        return res.status(400).json({
            status: "Bad request",
            message: "street is required !"
        })
    }
    if (!suite) {
        return res.status(400).json({
            status: "Bad request",
            message: "suite is required !"
        })
    }
    if (!city) {
        return res.status(400).json({
            status: "Bad request",
            message: "city is required !"
        })
    }
    if (!zipcode) {
        return res.status(400).json({
            status: "Bad request",
            message: "zipcode is required !"
        })
    }
    if (!lat) {
        return res.status(400).json({
            status: "Bad request",
            message: "lat is required !"
        })
    }
    if (!lng) {
        return res.status(400).json({
            status: "Bad request",
            message: "lng is required !"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required !"
        })
    }
    if (!website) {
        return res.status(400).json({
            status: "Bad request",
            message: "website is required !"
        })
    }
    if (!companyname) {
        return res.status(400).json({
            status: "Bad request",
            message: "companyName is required !"
        })
    }
    if (!catchPhrase) {
        return res.status(400).json({
            status: "Bad request",
            message: "catchPhrase is required !"
        })
    }
    if (!bs) {
        return res.status(400).json({
            status: "Bad request",
            message: "bs is required !"
        })
    }

    const newUserData = {
        _id: new mongoose.Types.ObjectId,
        name,
        username,
        email,
        address: {
            street,
            suite,
            city,
            zipcode,
            geo: {
                lat,
                lng
            }
        },
        phone,
        website,
        company: {
            name: companyname,
            catchPhrase,
            bs
        }
    }
    //thực thi model
    try {
        const createdUser = await userModel.create(newUserData);
        if (createdUser) {
            return res.status(201).json({
                status: "Create new user successfully !",
                data: createdUser
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all user:
const getAllUsers = async (req, res) => {
    try {
        const userList = await userModel.find();
        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all users successfully!",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user",
                data: userList

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//get user by id:
const getUserById = async (req, res) => {
    const userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid`
        })
    }
    try {
        const userFoundById = await userModel.findById(userId);
        if (userFoundById) {
            return res.status(200).json({
                status: `User found by Id ${userId} is successfully !`,
                data: userFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any users by id ${userId}`,
                data: userFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update user by id
const updateUserById = async (req, res) => {
    const userId = req.params.userId;
    const { name,
        username,
        email,
        address: {
            street,
            suite,
            city,
            zipcode,
            geo: {
                lat,
                lng
            }
        },
        phone,
        website,
        company: {
            companyname,
            catchPhrase,
            bs
        }
    } = req.body;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }
    // //B2: validate dữ liệu:
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required !"
        })
    }
    if (!username) {
        return res.status(400).json({
            status: "Bad request",
            message: "username is required !"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required !"
        })
    }
    if (!street) {
        return res.status(400).json({
            status: "Bad request",
            message: "street is required !"
        })
    }
    if (!suite) {
        return res.status(400).json({
            status: "Bad request",
            message: "suite is required !"
        })
    }
    if (!city) {
        return res.status(400).json({
            status: "Bad request",
            message: "city is required !"
        })
    }
    if (!zipcode) {
        return res.status(400).json({
            status: "Bad request",
            message: "zipcode is required !"
        })
    }
    if (!lat) {
        return res.status(400).json({
            status: "Bad request",
            message: "lat is required !"
        })
    }
    if (!lng) {
        return res.status(400).json({
            status: "Bad request",
            message: "lng is required !"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required !"
        })
    }
    if (!website) {
        return res.status(400).json({
            status: "Bad request",
            message: "website is required !"
        })
    }
    if (!companyname) {
        return res.status(400).json({
            status: "Bad request",
            message: "companyName is required !"
        })
    }
    if (!catchPhrase) {
        return res.status(400).json({
            status: "Bad request",
            message: "catchPhrase is required !"
        })
    }
    if (!bs) {
        return res.status(400).json({
            status: "Bad request",
            message: "bs is required !"
        })
    }
    const UserUpdateData = {
        name,
        username,
        email,
        address: {
            street,
            suite,
            city,
            zipcode,
            geo: {
                lat,
                lng
            }
        },
        phone,
        website,
        company: {
            name: companyname,
            catchPhrase,
            bs
        }
    }
    try {
        const userUpdated = await userModel.findByIdAndUpdate(userId, UserUpdateData);
        if (userUpdated) {
            return res.status(200).json({
                status: `Update user by id ${userId} successfully !`,
                data: userUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any users by Id ${userId}`,
                data: userUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//delete user by id
const deleteUserById = async (req, res) => {
    const userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }
    try {
        const userDeleted = await userModel.findByIdAndDelete(userId)
        if (userDeleted) {
            return res.status(204).json({
                status: `Delete user by id ${userId} successfully`,
                data: userDeleted
            })
        } else {
            return res.status(404).json({
                status: `Not found any users by Id ${userId}`,
                data: userDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


module.exports = { createUser, getAllUsers, getUserById, updateUserById, deleteUserById }
