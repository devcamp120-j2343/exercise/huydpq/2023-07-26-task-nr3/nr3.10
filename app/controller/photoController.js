//Import model
const { default: mongoose } = require('mongoose');
const photoModel = require('../models/photoModel')
const albumModel = require("../models/albumModel")
//create new Photo
const createPhoto = async (req, res) => {
    //Thu thập dữ liệu:
    const {albumId, title, url, thumbnailUrl} = req.body;

    //validate dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Photo Id ${albumId} is invalid !`
        })
    }
    if (!title) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Title is required !`
        })
    }
    if (!url) {
        return res.status(400).json({
            status: `Bad request`,
            message: `url is required !`
        })
    }
    if (!thumbnailUrl) {
        return res.status(400).json({
            status: `Bad request`,
            message: `thumbnailUrl is required !`
        })
    }
    //thực thi model:
    const newPhotoCreateData = {
        _id: new mongoose.Types.ObjectId,
        albumId,
        title,
        url,
        thumbnailUrl
    }
    try {
        const album = await albumModel.findById(albumId)
        if (album) {
            const newPhotoCreated = await photoModel.create(newPhotoCreateData)
            return res.status(201).json({
                status: `Create new Photo successfully !`,
                data: newPhotoCreated
            })
        } else {
            return res.status(400).json({
                status: "Not found any Album"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all Photo:
const getAllPhotos = async (req, res) => {
    const photoId = req.query.photoId
    try {
        if (photoId) {
            const PhotoList = await photoModel.find({ photoId: photoId });
            if (PhotoList && PhotoList.length > 0) {
                return res.status(200).json({
                    status: "Get all Photos successfully!",
                    data: PhotoList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any Photos",
                    data: PhotoList

                })
            }
        }else{
            const PhotoList = await photoModel.find();
            if (PhotoList && PhotoList.length > 0) {
                return res.status(200).json({
                    status: "Get all Photos successfully!",
                    data: PhotoList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any Photos",
                    data: PhotoList

                })
            }
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get Photo by id:
const getPhotoById = async (req, res) => {
    const photoId = req.params.photoId;
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Photo Id ${photoId} is invalid`
        })
    }
    try {
        const photoFoundById = await photoModel.findById(photoId);
        if (photoFoundById) {
            return res.status(200).json({
                status: `Photo found by Id ${photoId} is successfully !`,
                data: photoFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Photos by id ${photoId}`,
                data: photoFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update Photo by id
const updatePhotoById = async (req, res) => {
    const photoId = req.params.photoId;
    const {PhotoId, title, url, thumbnailUrl} = req.body;
        //validate:

    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Photo Id ${photoId} is invalid !`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(PhotoId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Photo Id ${PhotoId} is invalid !`
        })
    }

    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required !"
        })
    }
    if (!url) {
        return res.status(400).json({
            status: `Bad request`,
            message: `url is required !`
        })
    }
    if (!thumbnailUrl) {
        return res.status(400).json({
            status: `Bad request`,
            message: `thumbnailUrl is required !`
        })
    }
    

    const photoUpdateData = {
        PhotoId,
        title,
        url,
        thumbnailUrl
        
    }
    try {
        const newPhotoUpdated = await photoModel.findByIdAndUpdate(photoId, photoUpdateData);
        if (newPhotoUpdated) {
            return res.status(200).json({
                status: `Update Photo by id ${photoId} successfully !`,
                data: newPhotoUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any Photos by Id ${photoId}`,
                data: newPhotoUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete Photo by id
const deletePhotoById = async (req, res) => {
    const photoId = req.params.photoId;
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Photo Id ${photoId} is invalid !`
        })
    }
    try {
        const photoDeleted = await photoModel.findByIdAndDelete(photoId)
        if (photoDeleted) {
            return res.status(204).json({
                status: `Delete Photo by id ${photoId} successfully`,
                data: photoDeleted
            })
        } else {
            return res.status(404).json({
                status: `Not found any Photos by Id ${photoId}`,
                data: photoDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {createPhoto, getAllPhotos, getPhotoById, updatePhotoById, deletePhotoById}