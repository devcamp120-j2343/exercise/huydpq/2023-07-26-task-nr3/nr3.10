//Khơi tạo mongoose:
const mongoose = require('mongoose');
//import model:
const commentModel = require('../models/commentModel')
const postModel = require("../models/postModel")
//Create new Comment:
const createNewComment = async (req, res) => {
    
    const {postId, name, email, body } = req.body;

    if (!postId) {
        return res.status(400).json({
            status: "Bad request",
            message: "postId is required !"
        })
    }
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required !"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required !"
        })
    }
    if (!body) {
        return res.status(400).json({
            status: "Bad request",
            message: "body is required !"
        })
    }
    //thực thi model:
    const newCommentCreateData = {
        _id: new mongoose.Types.ObjectId,
        postId,
        name,
        email,
        body
    }
    try {
        const post = await postModel.findById(postId)
        if (post) {
            const newCommentCreated = await commentModel.create(newCommentCreateData);

            return res.status(201).json({
                status: `Create new Comment of Post Id ${postId} successfully !`,
                data: newCommentCreated
            })
        } else {
            return res.status(404).json({
                status:"Not found any post"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

//get all Comment:
const getAllComments = async (req, res) => {
    const postId = req.query.postId
    try {
        if (postId) {
            const CommentList = await commentModel.find({ postId: postId });
            if (CommentList && CommentList.length > 0) {
                return res.status(200).json({
                    status: "Get all Comments successfully!",
                    data: CommentList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any Comments",
                    data: CommentList

                })
            }
        } else {
            const CommentList = await commentModel.find();
            if (CommentList && CommentList.length > 0) {
                return res.status(200).json({
                    status: "Get all Comments successfully!",
                    data: CommentList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any Comments",
                    data: CommentList

                })
            }
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get Comment by id:
const getCommentById = async (req, res) => {
    const commentId = req.params.commentId;
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Comment Id ${commentId} is invalid`
        })
    }
    try {
        const commentFoundById = await commentModel.findById(commentId);
        if (commentFoundById) {
            return res.status(200).json({
                status: `Comment found by Id ${commentId} is successfully !`,
                data: commentFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Comments by id ${commentId}`,
                data: commentFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update Comment by id
const updateCommentById = async (req, res) => {
    const commentId = req.params.commentId;
    const { name, email, body } = req.body;
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Comment Id ${commentId} is invalid !`
        })
    }
    //validate:
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required !"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required !"
        })
    }

    if (!body) {
        return res.status(400).json({
            status: "Bad request",
            message: "body is required !"
        })
    }
    const commentUpdateData = {
        name,
        email,
        body
    }
    try {
        const newCommentUpdated = await commentModel.findByIdAndUpdate(commentId, commentUpdateData);
        if (newCommentUpdated) {
            return res.status(200).json({
                status: `Update Comment by id ${commentId} successfully !`,
                data: newCommentUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any Comments by Id ${commentId}`,
                data: newCommentUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete Comment by id
const deleteCommentById = async (req, res) => {
    const commentId = req.params.commentId;
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Comment Id ${commentId} is invalid !`
        })
    }
    try {
        const commentDeleted = await commentModel.findByIdAndDelete(commentId)
        if (commentDeleted) {
            return res.status(204).json({
                status: `Delete Comment by id ${commentId} successfully`,
                data: commentDeleted
            })
        } else {
            return res.status(404).json({
                status: `Not found any Comments by Id ${commentId}`,
                data: commentDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
module.exports = { createNewComment, getAllComments, getCommentById, updateCommentById, deleteCommentById }
