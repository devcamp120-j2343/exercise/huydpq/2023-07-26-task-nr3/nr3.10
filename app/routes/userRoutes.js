const express = require("express");
const { createUser, getAllUsers, updateUserById, deleteUserById } = require("../controller/userController");

const userRouter = express.Router();

userRouter.get("/users",getAllUsers)

userRouter.get("/users/:userId",getAllUsers)

userRouter.post("/users",createUser)

userRouter.put("/users/:userId",updateUserById)

userRouter.delete("/users/:userId",deleteUserById)

module.exports = {userRouter}