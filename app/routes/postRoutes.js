const express = require("express");
const { createPost, getAllPosts, getPostById, updatePostById, deletePostById } = require("../controller/postController");

const postRouter = express.Router();

postRouter.post("/posts", createPost)

postRouter.get("/posts", getAllPosts)

postRouter.get("/posts/:postId", getPostById)

postRouter.put("/posts/:postId", updatePostById)

postRouter.get("/posts/:postId", deletePostById)



module.exports = {postRouter}