const express = require("express");
const { createNewComment, getAllComments, getCommentById, updateCommentById, deleteCommentById } = require("../controller/commentController");

const commentRouter =  express.Router();

commentRouter.post("/comments", createNewComment)

commentRouter.get("/comments", getAllComments)

commentRouter.get("/comments/:commentId", getCommentById)

commentRouter.put("/comments/:commentId", updateCommentById)

commentRouter.delete("/comments/:commentId", deleteCommentById)

module.exports = {commentRouter}