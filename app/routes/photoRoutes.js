const express = require("express");
const { createPhoto, getAllPhotos, getPhotoById, updatePhotoById, deletePhotoById } = require("../controller/photoController");

const photoRouter = express.Router();

photoRouter.post("/photos",createPhoto)

photoRouter.get("/photos",getAllPhotos)

photoRouter.get("/photos/:photoId",getPhotoById)

photoRouter.put("/photos/:photoId",updatePhotoById)

photoRouter.delete("/photos/:photoId",deletePhotoById)


module.exports = {photoRouter}