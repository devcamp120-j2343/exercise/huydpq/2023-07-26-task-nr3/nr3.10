const express = require("express");

const toDoRouter = express.Router();

toDoRouter.post("/todos",createPhoto)

toDoRouter.get("/todos",getAllPhotos)

toDoRouter.get("/todos/:todoId",getPhotoById)

toDoRouter.put("/todos/:todoId",updatePhotoById)

toDoRouter.delete("/todos/:todoId",deletePhotoById)


module.exports = {toDoRouter}