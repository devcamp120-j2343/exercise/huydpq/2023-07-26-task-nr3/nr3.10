const express = require("express");
const { createAlbum, getAllAlbums, getAlbumById, updateAlbumById, deleteAlbumById } = require("../controller/albumController");

const albumRouter = express.Router();

albumRouter.post("/albums",createAlbum)

albumRouter.get("/albums",getAllAlbums)

albumRouter.get("/albums/:albumId",getAlbumById)

albumRouter.put("/albums/:albumId",updateAlbumById)

albumRouter.delete("/albums/:albumId",deleteAlbumById)


module.exports = {albumRouter}