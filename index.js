// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Import mongooseJS
const mongoose = require("mongoose");

const userModel = require("./app/models/userModel");
const postModel = require("./app/models/postModel");
const commentModel = require("./app/models/commentModel");
const albumModel = require("./app/models/albumModel");
const photoModel = require("./app/models/photoModel");
const todoModel = require("./app/models/todoModel");
//
const { userRouter } = require("./app/routes/userRoutes");
const { postRouter } = require("./app/routes/postRoutes");
const { commentRouter } = require("./app/routes/commentRoutes");
const { albumRouter } = require("./app/routes/albumRoutes");
const { photoRouter } = require("./app/routes/photoRoutes");
const { toDoRouter } = require("./app/routes/toDoRoutes");

// Khởi tạo app express
const app = express();

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

// Khai báo cổng của project
const port = 8000;

mongoose.connect("mongodb://127.0.0.1:27017/Zigvy_Interview")
.then(() => console.log("Connected to Mongo Successfully"))
.catch(error => handleError(error));
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
}
)

app.use('/', userRouter)
app.use('/', postRouter)
app.use('/', commentRouter)
app.use('/', albumRouter)
app.use('/', photoRouter)
app.use('/', toDoRouter)

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

// Chạy app express
app.listen(port, () => {
    console.log("App listening on port " + port);
})